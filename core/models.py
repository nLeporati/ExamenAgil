from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

PROFILE_SEXOS = (
    ('Masculino', 'Masculino'),
    ('Femenino', 'Femenino'), 
)

CREDITCARD_TIPOS = (
    ('VISA', 'VISA'),
    ('MASTERCARD', 'MASTERCARD'),   
)

PARKINGRENT_ESTADOS = (
    ('RENT', 'En eenta'),
    ('END', 'Finalizado'),
    ('CANCELED', 'Cancelado'),
    ('PENDING', 'Pago pendiente'),
    ('PAID', 'Pagado'),
)

class Profile(models.Model):
    nombre = models.CharField(max_length=80, null=True, blank=True)
    direccion = models.CharField(max_length=100, null=True, blank=True)
    ciudad = models.CharField(max_length=100, null=True, blank=True)
    celular = models.IntegerField(null=True, blank=True)
    sexo = models.CharField(max_length=30, choices=PROFILE_SEXOS, null=True, blank=True)
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        if self.nombre is None:
            return self.usuario.username
        else:
            return self.nombre

class CreditCard(models.Model):
    tipo = models.CharField(max_length=20, choices=CREDITCARD_TIPOS, null=True)
    nombre = models.CharField(max_length=80, null=True)
    numero = models.IntegerField(null=True)
    fechaVencimiento = models.DateField(null=True)
    cvv = models.IntegerField(null=True)
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        if self.numero is None:
            return self.usuario.username
        else:
            return str(self.numero)

class Parking(models.Model):
    direccion = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=80)
    pais = models.CharField(max_length=80)
    geo_x = models.FloatField()
    geo_y = models.FloatField()
    valor = models.IntegerField()
    propietario = models.ForeignKey(User, on_delete=models.CASCADE)
    disponible = models.BooleanField(default=True)

    def __str__(self):
        return self.direccion + ', ' + self.ciudad + ', ' + self.pais + '.'

class ParkingRent(models.Model):
    arrendador = models.ForeignKey(User, on_delete=models.CASCADE)
    arrendatario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="arrendatario")
    fecha = models.DateTimeField(auto_now=True)
    estado = models.CharField(max_length=20, choices=PARKINGRENT_ESTADOS, default="RENT")
    duracion = models.IntegerField(null=True, blank=True)
    valor = models.IntegerField(null=True, blank=True)
    parking = models.ForeignKey(Parking, on_delete=models.CASCADE)

    def __str__(self):
        return self.arrendatario.username + ' ' + str(self.fecha) + ' - ' + self.estado


# Recivers

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(usuario=instance)
        CreditCard.objects.create(usuario=instance)
