from django.urls import path
from django.contrib.auth.views import LoginView #, logout_then_login
# from django.contrib.auth.decorators import login_required
# from django.conf import settings
# from django.contrib.auth import views as views_auth
from . import views

app_name = 'core'

urlpatterns = [
    path('', views.login, name="login"),
    path('index', views.home, name="index"),
    path('login', LoginView.as_view(template_name='core/login.html'), name="login"),
    path('signup/arrendatario', views.SignupUserArrendatario.as_view(), name="signup_arrendatario"),
    path('signup/arrendador', views.SignupUserArrendador.as_view(), name="signup_arrendador"),
    path('profile/', views.ProfileUpdate.as_view(), name="profile"),
    path('profile/credit-card', views.CreditCardUpdate.as_view(), name="credit-card"),
    path('parking', views.ParkingList.as_view(), name="parking_list"),
    path('parking/new', views.ParkingCreate.as_view(), name="parking_new"),
    path('parking/<int:pk>', views.ParkingUpdate.as_view(), name="parking_update"),
    path('parking/<int:pk>/delete', views.ParkingDelete.as_view(), name="parking_delete"),
    path('rent', views.rent, name="rent"),

    # path('login/', LoginView.as_view(template_name='login.html'), name="login"),
    # path('logout/', views_auth.logout_then_login, {'login_url': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    # path('registro/', RegistroUsuario.as_view(), name="registro"),
]
