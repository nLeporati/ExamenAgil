from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import Profile, CreditCard, Parking, PROFILE_SEXOS, CREDITCARD_TIPOS, ParkingRent

class SignupForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = {
            'username',
            'email',
            'password1',
            'password2',
            }
        labels = {
            'username':'Nombre de Usuario',
            'email':'Email',
            'password1':'Contraseña',
            'password2':'Repita Contraseña',
            }

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['usuario']
        widgets = {
            'nombre' : forms.TextInput(attrs={'type': 'text','class':'form-control'}),
            'direccion' : forms.TextInput(attrs={'type': 'text','class':'form-control'}),
            'ciudad' : forms.TextInput(attrs={'type': 'text','class':'form-control'}),
            'celular' : forms.TextInput(attrs={'type': 'text','class':'form-control'}),
            'sexo' : forms.Select(attrs={'class':'form-control'}, choices=PROFILE_SEXOS),
        }

class CreditCardForm(forms.ModelForm):
    class Meta:
        model = CreditCard
        fields = '__all__'
        exclude = ['usuario']
        widgets = {
            'nombre' : forms.TextInput(attrs={'type': 'text','class':'form-control'}),
            'numero' : forms.NumberInput(attrs={'type': 'text','class':'form-control'}),
            'cvv' : forms.TextInput(attrs={'type': 'text','class':'form-control','maxlength':'3'}),
            'tipo' : forms.Select(attrs={'class':'form-control','placeholder':'MM/YY'}, choices=CREDITCARD_TIPOS),
        }

class ParkingForm(forms.ModelForm):
    geo_x = forms.CharField(required=False)
    geo_y = forms.CharField(required=False)

    class Meta:
        model = Parking
        fields = '__all__'
        exclude = ['propietario', 'disponible']
        widgets = {
            'direccion' : forms.TextInput(attrs={'type': 'text','class':'form-control','placeholder':'Calle Nº'}),
            'ciudad' : forms.NumberInput(attrs={'type': 'text','class':'form-control','value':'Santiago'}),
            'pais' : forms.TextInput(attrs={'type': 'text','class':'form-control','value':'Chile'}),
            'valor' : forms.NumberInput(attrs={'type': 'number','class':'form-control'}),
        }

class RentForm(forms.ModelForm):
    class Meta:
        model = ParkingRent
        fields = ['estado']

