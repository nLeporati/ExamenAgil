from django.contrib import admin
from . import models

# Register your models here.

admin.site.register(models.Profile)
admin.site.register(models.CreditCard)
admin.site.register(models.Parking)
admin.site.register(models.ParkingRent)
