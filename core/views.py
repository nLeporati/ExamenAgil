from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.urls import reverse_lazy
from .forms import SignupForm, ProfileForm, CreditCardForm, ParkingForm, RentForm
from .models import Profile, CreditCard, Parking, ParkingRent
from .decorators import group_required

# Create your views here.

@login_required
def home(request):
    group = request.user.groups.filter(name='Arrendatario').exists()
    if group:
        return redirect('core:rent')
    else:
        return redirect('core:parking_list')

def login(request):
    return render(request, 'core/login.html')

class SignupUserArrendatario(LoginRequiredMixin, CreateView):
    model = get_user_model()
    template_name = "core/signup.html"
    form_class = SignupForm
    success_url = reverse_lazy('core:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['role']='Arrendatario'
        return context

    def form_valid(self, form):
        self.object = form.save()
        self.object.groups.add(Group.objects.get(name='Arrendatario'))
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class SignupUserArrendador(CreateView):
    model = get_user_model()
    template_name = "core/signup.html"
    form_class = SignupForm
    success_url = reverse_lazy('core:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['role']='Arrendador'
        return context

    def form_valid(self, form):
        self.object = form.save()
        self.object.groups.add(Group.objects.get(name='Arrendador'))
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class ProfileUpdate(UpdateView):
    model = Profile
    template_name = "core/profile.html"
    form_class = ProfileForm
    success_url = reverse_lazy('core:profile')

    def get_object(self):
        return Profile.objects.get(usuario=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['creditCard']=CreditCard.objects.get(usuario=self.request.user)
        return context

class CreditCardUpdate(UpdateView):
    model = CreditCard
    template_name = "core/credit-card.html"
    form_class = CreditCardForm
    success_url = reverse_lazy('core:profile')

    def get_object(self):
        return CreditCard.objects.get(usuario=self.request.user)
    
    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST['fechaVencimiento'] = "01/" + request.POST['fechaVencimiento']
        return super(CreditCardUpdate, self).post(request, **kwargs)

class ParkingList(ListView):
    model = Parking
    template_name = "core/parking_list.html"
    context_object_name = "parkings"

    def get_queryset(self):
        return Parking.objects.filter(propietario=self.request.user)

class ParkingCreate(CreateView):
    model = Parking
    template_name = "core/parking_form.html"
    form_class = ParkingForm
    success_url = reverse_lazy('core:parking_new')
    
    def form_valid(self, form):
        form.instance.propietario = self.request.user
        return super(ParkingCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['creditCard']=CreditCard.objects.get(usuario=self.request.user)
        return context


class ParkingUpdate(UpdateView):
    model = Parking
    template_name = "core/parking_update.html"
    context_object_name = 'parking'
    form_class = ParkingForm
    success_url = reverse_lazy('core:parking_list')

class ParkingDelete(DeleteView):
    model = Parking
    template_name = "core/parking_delete.html"
    context_object_name = 'parking'
    success_url = reverse_lazy('core:parking_list')

class Rent(CreateView):
    model = ParkingRent
    template_name = "core/home.html"
    # form_class = RentForm
    success_url = reverse_lazy('core:login')

    def get_queryset(self):
        return ParkingRent.objects.filter(propietario=self.request.user, )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rent']=ParkingRent.objects.get(arrendatario=self.request.user)
        return context

@login_required
def rent(request):
    rents = ParkingRent.objects.filter(arrendatario=request.user)
    parkings = Parking.objects.filter(disponible=True)
    creditCard = CreditCard.objects.filter(usuario=request.user)

    if request.POST:
        if request.POST.get('parking', None) != None:
            parking = Parking.objects.get(pk=request.POST['parking'])
            rent = ParkingRent()
            rent.parking = parking
            rent.arrendador = parking.propietario
            rent.arrendatario = request.user
            rent.save()
            parking.disponible = False
            parking.save()

        if request.POST.get('remove', None) != None:
            rent = ParkingRent.objects.get(pk=request.POST['remove'])
            parking = Parking.objects.get(pk=rent.parking.pk)
            diff = timezone.now() - rent.fecha
            mins = round(diff.days * 1440 + diff.seconds/60)
            rent.valor = 0
            rent.duracion = mins
            rent.estado = "CANCELED"
            rent.save()            
            parking.disponible = True
            parking.save()

        if request.POST.get('payment', None) != None:
            rent = ParkingRent.objects.get(pk=request.POST['payment'])
            parking = Parking.objects.get(pk=rent.parking.pk)
            diff = timezone.now() - rent.fecha
            mins = round(diff.days * 1440 + diff.seconds/60)
            cost = mins * parking.valor
            rent.valor = cost
            rent.duracion = mins
            rent.estado = "PAID"
            rent.save()
            parking.disponible = True
            parking.save()

    context = {
        'rents': rents,
        'parkings': parkings,
        'creditCard': creditCard
    }
    return render(request, 'core/home.html', context)